+++
author = "Goutham Krishna K V"
title = "Session 1"
date = 2021-04-18T01:07:47+05:30
description = "Session 1 - Setting up your Environment"
tags = [
    "c++",
    "linux",
    "bash",
    "powershell",
    "windows"
]
+++

In this session we are going to set up our system to learn C++.
<!--more-->


This document describes in detail, setting up your computer to enable
you to learn C++ for Let's Code. This covers Windows and UNIX-based
(MacOS & Linux) Operating Systems. For this, we would prefer you use
UNIX-based operating systems (most preferably Linux Distributions like
[[Ubuntu]](https://ubuntu.com/),
[[Fedora]](https://getfedora.org/),
[[Pop!\_OS]](https://system76.com/pop),
[[Manjaro]](https://manjaro.org/) etc.), but Windows also
works, though only older compilers are available for Windows compared to
UNIX-based Operating Systems.

For Learning C++ (and for the sake of similar behavior across platforms)
**we are going to be using the C++ Compiler from [[GNU Compiler
Collection]](https://gcc.gnu.org/)** (g++) as **it works
similarly in Windows, MacOS and Linux**. In MacOS it's easier to install
LLVM Compilers and it's a drop-in replacement for GCC, so we're gonna
stick with LLVM Clang Compilers for MacOS.

We are going to be doing the following things to install and check it.

1.  Installing the [[GCC C++
    Compiler]](https://gcc.gnu.org/). This compiles
    ('builds') your C++ code.

2.  Installing the text editor, [[Visual Studio
    Code]](https://code.visualstudio.com/). This helps you
    write C++ code.



Installing the GCC C++ Compiler
===============================

Windows
-------

1.  Right-click on Start-Menu Icon on the bottom-corner. That would open
    a context menu with a few options. In the context-menu, click
    **Windows PowerShell (Admin)**.

![](/assets/session_1/image13.png)
>
This opens up an **Administrator PowerShell Window**, which looks like
this.
>
![](/assets/session_1/image1.png)
  ----------------------------------------------------------------------------------------------------------------------------------------------
  **WHAT WE DID**: We opened the Windows Command-Line window to assist us in installing the package manager, which in this case is Chocolatey.
  ----------------------------------------------------------------------------------------------------------------------------------------------

1.  Copy the below text and paste it into the PowerShell Window, and
    Click **Enter**.

  ```
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  ```

This would Install Chocolatey Package Manager in your Computer and set
up the system for installing packaged programs in Chocolatey.
>
When you get a prompt again, (check whether the last line looks like
the text inside the highlighted box above).
>
![](/assets/session_1/image9.png)

**Note:**  
There maybe errors when running the previous command especially in older windows like Windows 7 because of older powershell version. If you face an error, follow the below steps to install `chocolatey` : 


{{< gist afeedh 4e969bd7c689d047c8d3389972876167 >}}



1.  To make sure chocolatey works, run the following command.

  ```
  choco -v
  ```

It **should** show the following output.
>
![](/assets/session_1/image10.png)

2.  Run the following command to install the GCC C++ Compiler, provided
    by [[MinGW
    Project]](https://sourceforge.net/projects/mingw-w64/)
    (Minimalist GNU for Windows) to your computer.

  ```
  choco install mingw -y
  ```

This would download the MinGW Compiler Collection (\~50MB) and install
all tools and compilers necessary for C++ development (and more).
>
Make sure after execution, the last few lines look similar to this.
>
![](/assets/session_1/image11.png)
>
The output states that the package "mingw" is installed, which itself
comes with a host of compilers for *C, C++, D, Fortran, Ada, GNU
Assembler* etc, along with a few related tools.

3.  Confirm that C++ Compiler (g++) is installed by using the following
    command.

  ```
  g++ -v
  ```

This should output the following.
>
![](/assets/session_1/image2.png)
>
In this, the last line shows the version of C++ Compiler from GCC,
which is GCC C++ Compiler 10.2.0.
>
This gives us a confirmation that the compiler installed successfully.
>
NOTE: If you want to install Visual Studio Code using the same way,
there's no need to close this window. You can continue on here.

**GCC is Installed**

MacOS
-----

Now in this case, we won't be using the GCC Compiler as LLVM Compiler
comes by default in MacOS XCode IDE, and has feature-parity with GCC.

1.  Go to the Mac App Store and Install XCode.

2.  Open a Terminal window and run the following command.

  ```
  g++ -v
  ```

If it provides an output which contains the keyword clang, you've
installed it properly.

**LLVM (drop-in replacement for GCC) is installed**

Linux
-----

In this case, **everything we need is available from their respective
package repositories** (a central location where they keep all software,
***kind of like an App Store***). Ubuntu and Debian-based distributions
use apt, Fedora and most Red Hat-based distributions use dnf and Manjaro
and Arch Linux based distributions use pacman Installer.

1.  Launch the Terminal Application and paste the command which is
    relevant to your Linux Distribution

#### Ubuntu, ElementaryOS, Debian-based Distributions

  ```
  sudo apt update && sudo apt install build-essential -y
  ```

#### Fedora, Red Hat Enterprise Linux, CentOS

``` 
sudo dnf groupinstall \"C Development Tools and Libraries\" -y
```

#### Manjaro and other Arch Linux based distributions

```
sudo pacman -Syu base-devel
```

The terminal would ask to provide your password. **Type your user
password** and click **Enter**.
>
![](/assets/session_1/image14.png)
>
Wait for the command to finish execution. When the installation
completes, you'll see the prompt again.
>
![](/assets/session_1/image12.png)

1.  Check whether the compiler is installed correctly by running the
    following command

  ```
  g++ -v
  ```

It should give you an output that's similar to the following
>
![](/assets/session_1/image16.png)
>
In the last line, any version of GCC above 8.0.0 is a recent enough
compiler.

**GCC is Installed**

By this time you should have successfully installed the GCC C++ Compiler
(LLVM for MacOS).

Installing Visual Studio Code
=============================

Visual Studio Code (also called VS Code or just "code") is a
cross-platform code editor from Microsoft which has a lot of features
and flexibility for getting work done, and it's easy usability.

Windows
-------

Now, there are two ways to install Visual Studio Code, one uses the
Chocolatey Package Manager and in the other one you need to download it
from the Visual Studio Code Website.

### Using Chocolatey

Assuming you haven't closed your Administrator PowerShell window from
the previous step, run the following command

 
```
choco install vscode -y
```
 
This should install Visual Studio Code the way we need.

*NOTE: Though you are using the chocolatey package manager, there are
**no** differences between installing from Chocolatey nor from Official
Website*

### Using Official Website

1.  Go to the official website,
    [[code.visualstudio.com]](https://code.visualstudio.com/).
    And click *Download* in the top-right corner.

![](/assets/session_1/image18.png)

1.  In the following download page, Pick **System Installer 64-bit**
    option to download the installer.

![](/assets/session_1/image3.png)

1.  After downloading the installer, **open it and follow through to
    install the application**.

![](/assets/session_1/image19.png)

MacOS
-----

Follow the install instructions given in the official website
[[here]](https://code.visualstudio.com/docs/setup/mac).

Linux
-----

Follow the install instructions given in the official website
[[here]](https://code.visualstudio.com/docs/setup/linux).

NOTE: Only stick with ***Snap Store*** VSCode if you're using Ubuntu,
for all other distributions, refer to the instructions that follow it.

Setting up Visual Studio Code for C++ Programming
=================================================

To set Visual Studio Code for C++ Programming, we require a few
extensions in Visual Studio Code to be installed.

1.  Open Visual Studio Code. You would be presented with the following
    screen.

![](/assets/session_1/image15.png)

1.  Click Ctrl + Shift + X to open the **Extensions** panel.

Install the Following Extensions:

a.  ***C/C++ Extension* by *Microsoft***

![](/assets/session_1/image5.png)

b.  ***Code Runner* by *Jun Han***

![](/assets/session_1/image7.png)

The instructions above sets your computer up for writing proper C++
code.

Now to run a C++ program, all you have to do is open the program in
Visual Studio Code and click the "Play" button in the top right corner.

![](/assets/session_1/image8.png)

This would compile, and if compile succeeds, execute your program.


The Complete Setup Guide can be found [**here**](https://docs.google.com/document/d/1Y_NwygTnFC5u6KCzwg1eQIsuYz6F5w6xROJ1-a8DLlA/view?usp=sharing).