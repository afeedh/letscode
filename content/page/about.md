---
title: 'About Us'
button: 'About us'
weight: 2
---

Let's Code is an initiative by the Students Affair Council (SAC) of NIT Calicut which is conducted in association with Computer Science and Engineering Association (CSEA) and FOSSCell to help students from non-CS branches learn problem solving and coding.

