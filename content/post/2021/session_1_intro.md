+++
author = "Namburi Soujanya"
title = "Session 2"
date = 2021-04-24T01:11:05+05:30
description = "Session 2 - Basics of C++"
tags = [
    "shortcodes",
    "privacy",
]
+++


LET'S CODE - INTRODUCTORY SESSION
<!--more-->
## TOPIC 1:  C++ SYNTAX

**Example :**

```cpp
#include <iostream>
using namespace std;

int main() {
  cout << "Hello World!";
  return 0;
}
```
### **Breakdown :**

**Line 1:**
```cpp
#include <iostream>
```
It is a header file library that lets us work with input and output objects. These header files add functionality to the C++ programs

**Line 2:**
```cpp
using namespace std
```
means that we can use names for objects and variables from the standard library. You might see some C++ programs that runs without the standard namespace library. The using namespace std line can be omitted and replaced with the std keyword, followed by the :: operator for some objects:

Example: Use
```cpp
std::cout << "Hello World!"; 
```
instead of 
```cpp
cout << "Hello World!";
```
if line 2 is not added.

**Line 3:**

White spaces are omitted by C++
The compiler ignores white spaces. However, multiple lines makes the code more readable.

**Line 4:**

Another thing that always appear in a C++ program, is int main(). This is called a function. Any code inside its curly brackets {} will be executed.

**Line 5:**
cout (pronounced "see-out") is an object used together with the insertion operator (<<) to output/print text. In our example it will output "Hello World".

Note: Every C++ statement ends with a semicolon ;.


**Line 6:**

return 0 ends the main function.

**Line 7:**

Do not forget to add the closing curly bracket } to actually end the main function


## TOPIC 2: DATA TYPES

Variables are containers to store data values
In C++, there are different types of variables (defined with different keywords), They are: 

| DATA TYPE | SIZE    | DESCRIPTION                                                                                          |
| --------- | ------- | ---------------------------------------------------------------------------------------------------- |
| `int`       | 4 bytes | Stores whole numbers, without decimals                                                               |
| `float`     | 4 bytes | Stores fractional numbers, containing one or more decimals. Sufficient for storing 7 decimal digits  |
| `double`    | 8 bytes | Stores fractional numbers, containing one or more decimals. Sufficient for storing 15 decimal digits |
| `boolean`   | 1 bytes | Stores true or false values                                                                          |
| `char`      | 1 bytes | Stores a single character/letter/number, or ASCII values                                             |

### **Type Modifiers**

There are 4 types of Type Modifiers:
1. signed
2. unsigned
3. short
4. long

These change the range of values that variables can hold.

### ***Example***
```cpp
void main()
{
  int x;              //Range: -2147483648 to 2147483647
  unsigned int y;     //Range: 0 to 4294967295
  short int z;        //Range: -32768 to 32767
  long int w;         //Range: -2,147,483,648 to 2,147,483,647 
}
```

### **Declaring a Variable**

To declare a variable, you must specify the type and unique name to the variable.

```cpp
type variable;
```
Optionally, you can assign a value to the variable at the time of declaration.
```cpp
type variable = value;
```
Examples: 
```cpp

int myInt = 5;               // Integer (whole number without decimals)
double myFloatNum = 5.99;    // Floating point number (with decimals)
char myLetter = 'D';         // Character
string myText = "Hello";     // String (text)
bool myBoolean = true;       // Boolean (true or false)

int myNum;                   // Declare the variable
myNum = 666;                 // Assign the value later

cout << "I have " << myInt << " apples with me.";

int a = 4, b = 5, c = 100     // Declare multiple variables at the same time
```

All C++ variables must be identified with unique names.

These unique names are called identifiers.

Identifiers can be short names (like x and y) or more descriptive names (age, sum, totalVolume).

**Note:** It is recommended to use descriptive names in order to create understandable and maintainable code:

_*The general rules for constructing names for variables (unique identifiers) are:*_

1. Names can contain letters, digits and underscores
2. Names must begin with a letter or an underscore (_)
3. Names are case sensitive (myVar and myvar are different variables)
4. Names cannot contain whitespaces or special characters like !, #, %, etc.
5. Reserved words (like C++ keywords, such as int) cannot be used as identifiers.

## TOPIC 3: SCOPE OF VARIABLES
The scope of a variable is the part of the program it can be accessed from.

There are 2 types of scopes:
1. Global Scope
2. Local Scope
```cpp
#include <iostream>

using namespace std;
int global = 5; //This variable has global scope
int main()
{
    int x = 10; //This variable only has a local scope of only this main() function
    cout << "Global=" << global << endl
         << "x=" << x << endl;
    {
        int y = 7; // This variable has a local to only the enclosing braces.
        cout << "Global=" << global << endl
             << "x=" << x << endl
             << "y=" << y << endl;
    }
    //  cout<<"y="<<y;

    return 0;
}
```
## TOPIC 4: OPERATORS
Operators are used to perform operations on variables and values.

C++ divides the operators into the following groups:
1. Arithmetic operators
2. Assignment operators
3. Comparison operators
4. Logical operators
5. Bitwise operators

### **Arithmetic Operators:**

| Operator   | Name           | Description                            | Example    |
| ---------- | -------------- | -------------------------------------- | ---------- |
| `+`        | Addition       | Adds together two values               | `x + y`    |
| `-`        | Subtraction    | Subtracts one value from another       | `x - y`    |
| `*`        | Multiplication | Multiplies two values                  | `x * y`    |
| `/`        | Division       | Divides one value by another           | `x / y`    |
| `%`        | Modulus        | Returns the division remainder         | `x % y`    |
| `++`       | Increment      | Increases the value of a variable by 1 | `++x`      |
| `--`       | Decrement      | Decreases the value of a variable by 1 | `--x`      |

### **Example**
```cpp
int sum1 = 100 + 50;        // 150 (100 + 50)
int sum2 = sum1 + 250;      // 400 (150 + 250)
int sum3 = sum2 + sum2;     // 800 (400 + 400)
```

### **Assignment Operators:**

Assignment operators are used to assign values to variables.
| Operator | Example   | Meaning      |
| -------- | --------- | ------------ |
| `=`      | `x += 3`  | `x = x + 3`  |
| `-=`     | `x -= 3`  | `x = x - 3`  |
| `*=`     | `x *= 3`  | `x = x * 3`  |
| `/=`     | `x /= 3`  | `x = x / 3`  |
| `%=`     | `x %= 3`  | `x = x % 3`  |
| `&=`     | `x &= 3`  | `x = x & 3`  |
| `= `     | `x \|= 3` | `x = x \| 3` |
| `^=`     | `x ^= 3`  | `x = x ^ 3`  |
| `>>=`    | `x >>= 3` | `x = x >> 3` |
| `<<=`    | `x <<= 3` | `x = x << 3` |

### **Example**
```cpp
int x = 10;
x += 5;
```

### **Comparision Operators:**

Comparison operators are used to compare two values.
The return value of a comparison is either true (1) or false (0) - boolean values

| Operator | Name                     | Example |
| -------- | ------------------------ | ------- |
| `==`     | Equal to                 | `x == y`  |
| `!=`     | Not equal                | `x != y`  |
| `>`      | Greater than             | `x > y`   |
| `<`      | Less than                | `x < y`   |
| `>=`     | Greater than or equal to | `x >= y`  |
| `<=`     | Less than or equal to    | `x <= y`  |

### **Example**
```cpp
int y = 44;
cout << (x > y); // returns 1 (true) because 5 is greater than 3
```

### **Logical Operators:**
Logical operators are used to determine the logic between variables or values:

| Operator | Name        | Description                                             | Example            |
| -------- | ----------- | ------------------------------------------------------- | ------------------ |
| `&&`       | Logical and | Returns true if both statements are true                | `x < 5 &&  x < 10`   |
| `\|\|`     | Logical or  | Returns true if one of the statements is true           | `x < 5`              |
| `!`        | Logical not | Reverse the result, returns false if the result is true | `!(x < 5 && x < 10)` |

### **Example**
```cpp
#include <iostream>
using namespace std;

int main() {
  int x = 5;
  int y = 3;
  cout << (x > 3 && x < 10); // returns true (1) because 5 is greater than 3 AND 5 is less than 10
  return 0;
}
```
## TOPIC 5: TYPE CONVERSION

Types of Type Conversionss:
1. Implicit Type Conversion
2. Explicit Type Conversion
   
### Implicit Type Conversion
These type conversions are done automically by the compiler without the user specifying.

Order of Type  Conversions:
```cpp
bool -> char -> short int -> int -> unsigned int -> long -> unsigned -> long long -> float -> double -> long double
```
#### ***Example***
```cpp
void main()
{
  char x='d';     //ASCII value of 'd'=100
  int sum,y=5;
  sum=x+y;
  cout<<"The sum is="<<sum;
}
```

### Explicit Type Conversion

These Type conversions are explcitly specified by the user.

#### ***Example***
```cpp
void main()
{
  float x=10.5,y=5.5,sum;
  sum=x+(int)y;
  cout<<"The sum is="<<sum;
}
```

## TOPIC 6: CONDITIONAL STATEMENTS

Conditional statements are used to alter the flow of the program.
There are 4 types of conditional statements:
1. if statements
2. if-else statements
3. if-else-if ladder
4. switch statements
   
### **if Statement**
### Syntax:
```cpp
if(condition)
{
  /*code*/
}
```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x;
    cout<<"enter a number:";
    cin>>x;
    if(x>10)
    {
        cout<<"The entered number is greater than 10"<<endl;
    }
    cout<<"end of the program"<<endl;

    return 0;
}
```
### **if-else Statement**
### Syntax:
```cpp
if(condition)
{
  /*code*/
}
else
{
  /*code*/
}
```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x;
    cout << "enter a number:";
    cin >> x;
    if (x > 10)
    {
        cout << "The entered number is greater than 10" << endl;
    }
    else
    {
        cout << "The entered number is less than 10" << endl
    }
    cout << "end of the program" << endl;

    return 0;
}
```

### **if-else if ladder**
### Syntax:
```cpp
if(condition1)
{
  /*code*/
}
else if(condition2)
{
  /*code*/
}
else
{
  /*code*/
}
```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x;
    cout << "enter a number:";
    cin >> x;
    if (x > 3)
    {
        cout << "The entered number is greater than 10" << endl;
    }
    else if (x > 7)
    {
        cout << "The entered number is between 5&7" << endl;
    }
    else
    {
        cout << "The number entered in greater than 7" << endl;
    }
    cout << "end of the program" << endl;

    return 0;
}
```
### **if-else if ladder**
### Syntax:
```cpp
switch(expression)
{
  case <constant value>:
    /*code*/
    break;
.
.
.    
  case <constant value>:
      /*code*/
      break;
  default:
      /*code*/
      break;
}

```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x;
    cout << "enter a number:";
    cin >> x;
    switch (x)
    {
    case 1:
        cout << "value entered is 1" << endl;
        break;
    case 2:
        cout << "Entered value is 2" << endl;
        break;
    default:
        cout << "Entered value is greater than 2" << endl;
        break;
    }

    return 0;
}
```
## TOPIC 6: LOOP STATEMENTS

Loop statements are used to repeat certain statements until a condiion is satisfied.

There are 3 types of loop statements:
1. while loop
2. for loop
3. do...while loop
   
### **While Loop**
### Syntax:
```cpp
while(condition)
{
  /*code*/
}

```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x = 0;
    while (x < 20)
    {
        cout << x << endl;
        x++;
    }

    return 0;
}
```
### **for Loop**
### Syntax:
```cpp
for (initializations ; condtion ; updation)
{
  /*code*/
}

```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    for (int i = 0; i < 20; i++)
    {
        cout << i << endl;
    }

    return 0;
}
```
### **do..while Loop**
### Syntax:
```cpp
do
{
  /*code*/
} while (condition);

```
### Example:
```cpp
#include <iostream>

using namespace std;
int main()
{
    int x = 0;
    do
    {
        cout << x << endl;
        x++;
    } while (x < 20);

    return 0;
}
```
